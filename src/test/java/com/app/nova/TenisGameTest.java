package com.app.nova;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import com.app.nova.entity.Game;
import com.app.nova.entity.Player;
import com.app.nova.service.TennisGame;
import com.app.nova.util.Constants;


@RunWith(MockitoJUnitRunner.class)
public class TenisGameTest {

	List<Player> lstPlayer;
	
	@InjectMocks
	TennisGame tGame;
	
	@Before
	public void setUp() {
		lstPlayer = new ArrayList<Player>();
		Player j1=new Player(1, "Alonso");
		Player j2 = new Player(2, "Jenn");
		lstPlayer.add(j1);
		lstPlayer.add(j2);
	}
	
	@Test
	public void scoring_Test1() {
		
		Game g = new Game();
		g.iniciar(lstPlayer);
		tGame.scoring(g, lstPlayer, 1);
		assertTrue(lstPlayer.get(0).getPunt().getDescrScore().equals(Constants.STATUS.Fifteen.getMessage()));
		tGame.scoring(g, lstPlayer, 1);
		tGame.scoring(g, lstPlayer, 1);
		tGame.scoring(g, lstPlayer, 1);
		tGame.scoring(g, lstPlayer, 1);
		
		assertTrue(g.isFinish());
		assertTrue(lstPlayer.get(0).isWin());
		
	}
	
	@Test
	public void scoring_Test2() {
		
		Game g = new Game();
		g.iniciar(lstPlayer);
		tGame.scoring(g, lstPlayer, 1);
		tGame.scoring(g, lstPlayer, 2);
		tGame.scoring(g, lstPlayer, 2);
		tGame.scoring(g, lstPlayer, 1);
		tGame.scoring(g, lstPlayer, 2);
		tGame.scoring(g, lstPlayer, 1);
		tGame.scoring(g, lstPlayer, 1);
		tGame.scoring(g, lstPlayer, 2);
		
		assertTrue(!g.isFinish());
		assertTrue(g.isDeuce());
		
	}
	
	@Test
	public void scoring_Test3() {
		
		Game g = new Game();
		g.iniciar(lstPlayer);
		tGame.scoring(g, lstPlayer, 1);
		tGame.scoring(g, lstPlayer, 2);
		tGame.scoring(g, lstPlayer, 2);
		tGame.scoring(g, lstPlayer, 1);
		tGame.scoring(g, lstPlayer, 2);
		tGame.scoring(g, lstPlayer, 1);
		tGame.scoring(g, lstPlayer, 1);
		tGame.scoring(g, lstPlayer, 2);
		tGame.scoring(g, lstPlayer, 2);
		
		assertTrue(!g.isFinish());
		assertTrue(g.getPlayers().get(1).getPunt().isAdvantage());
		
	}
	
	@Test
	public void scoring_Test4() {
		
		Game g = new Game();
		g.iniciar(lstPlayer);
		tGame.scoring(g, lstPlayer, 1);
		tGame.scoring(g, lstPlayer, 2);
		tGame.scoring(g, lstPlayer, 2);
		tGame.scoring(g, lstPlayer, 1);
		tGame.scoring(g, lstPlayer, 2);
		tGame.scoring(g, lstPlayer, 1);
		tGame.scoring(g, lstPlayer, 1);
		tGame.scoring(g, lstPlayer, 2);
		tGame.scoring(g, lstPlayer, 2);
		tGame.scoring(g, lstPlayer, 2);
		
		assertTrue(g.isFinish());
		assertTrue(g.getPlayers().get(1).isWin());
		
	}
	
	@Test
	public void scoring_Test5() {
		
		Game g = new Game();
		g.iniciar(lstPlayer);
		tGame.scoring(g, lstPlayer, 1);
		tGame.scoring(g, lstPlayer, 2);
		tGame.scoring(g, lstPlayer, 2);
		tGame.scoring(g, lstPlayer, 1);
		tGame.scoring(g, lstPlayer, 2);
		tGame.scoring(g, lstPlayer, 1);
		tGame.scoring(g, lstPlayer, 1);
		tGame.scoring(g, lstPlayer, 2);
		tGame.scoring(g, lstPlayer, 2);
		tGame.scoring(g, lstPlayer, 2);
		
		assertTrue(g.isFinish());
		assertTrue(g.getPlayers().get(1).getPunt().getPunt()==g.getPlayers().get(0).getPunt().getPunt()+2);
		
	}
}
