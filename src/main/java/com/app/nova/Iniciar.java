package com.app.nova;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.app.nova.entity.Game;
import com.app.nova.entity.Player;
import com.app.nova.service.TennisGame;

public class Iniciar {

	public static void main(String[] args) {
		Scanner entradaEscaner = new Scanner(System.in);
		Game g= new Game();
		System.out.println("******* INICIANDO JUEGO **************");
		System.out.print("JUGADOR N°1 : ");
		Player a= new Player(1, entradaEscaner.nextLine());
		System.out.print("JUGADOR N°2 : ");
		Player b= new Player(2, entradaEscaner.nextLine());
		List<Player> lst = new ArrayList<Player>();
		lst.add(a);
		lst.add(b);
		g.iniciar(lst);
		
		TennisGame tg = new TennisGame();
		
		
		while(!g.isFinish()) {
			g.setDeuce(false);
			System.out.println("\n");
			System.out.print("JUGADORES -> 1- "+a.getName()+ "  2- "+b.getName() +" - ANOTO (1/2):  ");
			String anotacion="";
			anotacion = entradaEscaner.nextLine();
			try {
				int idA = Integer.parseInt(anotacion);
				tg.scoring(g,lst, idA);
				System.out.println("********** MARCADOR: "+ (g.isDeuce()?"Deuce":"") +( (lst.get(0).getPunt().isAdvantage()||lst.get(1).getPunt().isAdvantage())?"Adventage":"" )+" ***************");
				System.out.println("********** JUGADOR: "+ (idA==lst.get(0).getId()?lst.get(0).getName()+ "- "+lst.get(0).getPunt().getDescrScore()
						:lst.get(1).getName()+ "- "+lst.get(1).getPunt().getDescrScore()) +"***************");
				System.out.println(a.getName()+":"+a.getPunt().getPunt() + " - "+a.getPunt().isAdvantage() +" - "+a.getPunt().getScore()+" - "+a.getPunt().getDescrScore());
				System.out.println(b.getName()+":"+b.getPunt().getPunt()+ " - "+b.getPunt().isAdvantage() +" - "+b.getPunt().getScore()+" - "+b.getPunt().getDescrScore());
			}catch (Exception e) {
				// TODO: handle exception
				System.out.println("Ingrese el id del jugador (1/2)");
			}
			
		}
		System.out.println("******* JUEGO TERMINADO**************");
		System.out.println("**** GANADOR: "+(a.isWin()?a.getName():b.getName()));
	}
}
