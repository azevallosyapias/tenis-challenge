package com.app.nova.service;

import java.util.List;

import com.app.nova.entity.Game;
import com.app.nova.entity.Player;

public class TennisGame {

	/**
	 * <p> Metodo para calcular el score durante el juego de Tenis
	 * </p>
	 * @author Alonso
	 * @param Game: el juego en Curso, Lista de Jugadores, el Id del jugador que anoto 
	 * @return 
	 */
	
	public void scoring(Game game, List<Player> players, int id) {
		Player A = null;
		Player B = null;

		for (Player p : players) {
			if (p.getId() == id) {
				A = p;
			} else {
				B = p;
			}
		}

		int puntA = A.getPunt().getPunt();
		int puntB = B.getPunt().getPunt();

		puntA++;

		if (B.getPunt().isAdvantage()) {
			B.getPunt().updateAdv(false);
			game.setDeuce(true);
			A.getPunt().addScore();
		} else {

			if (A.getPunt().isAdvantage()) {
				game.setFinish(true);
				A.setWin(true);
				A.getPunt().addScore();
			} else {
				if (puntA > 2 && puntB > 2) {
					if (puntA - puntB > 0) {
						A.getPunt().setAdvantage(true);
						A.getPunt().addScore();
					} else if (puntA == puntB) {
						game.setDeuce(true);
						A.getPunt().addScore();
					}
				} else {
					if (puntA > 3) {
						A.setWin(true);
						game.setFinish(true);
					}
					A.getPunt().addScore();
				}
			}
		}
	}

}
