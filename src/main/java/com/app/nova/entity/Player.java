package com.app.nova.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class Player {

	@Getter	@Setter
	private int id;
	
	@Getter	@Setter
	private String name;
	
	@Getter	@Setter
	private Puntuation punt;
	
	@Getter	@Setter
	private boolean win;	
	
	public Player(int id,String name) {
		// TODO Auto-generated constructor stub
		this.id=id;
		this.name=name;
		this.win=false;
		this.punt=new Puntuation();
	}
	
}
