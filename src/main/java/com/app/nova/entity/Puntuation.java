package com.app.nova.entity;

import com.app.nova.util.Constants;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class Puntuation {

	@Getter @Setter
	private int punt;
	@Getter @Setter
	private int score;
	@Getter @Setter
	private String descrScore;
	
	@Getter	@Setter
	private boolean isAdvantage;
	
	public Puntuation() {
		this.punt=0;
		this.score=0;
		this.descrScore="Love";
		this.isAdvantage=false;
	}
	
	public void updateAdv(boolean adv) {
		this.isAdvantage=adv;
		if(!adv) {
			
			this.score=Constants.STATUS.Forty.getCodigo();
			this.descrScore=Constants.STATUS.Forty.getMessage();
		}else {
			this.punt++;
			this.score=Constants.STATUS.Advantage.getCodigo();
			this.descrScore=Constants.STATUS.Advantage.getMessage();
		}
	}
	
	public void addScore() {
		this.punt++;
		switch (this.score) {
		case 0:
			this.descrScore=Constants.STATUS.Fifteen.getMessage();
			this.score=Constants.STATUS.Fifteen.getCodigo();
			break;
		case 15:
			this.descrScore=Constants.STATUS.Thirty.getMessage();
			this.score=Constants.STATUS.Thirty.getCodigo();
			break;
		case 30:
			this.descrScore=Constants.STATUS.Forty.getMessage();
			this.score=Constants.STATUS.Forty.getCodigo();
			break;
		default:
			break;
		}
	}
	
	public void update(int punt, int score) {
		this.punt = punt;
		this.score = score;
		switch (this.score) {
		case 0:

			break;
		case 15:

			break;
		case 30:

			break;
		case 40:

			break;
		default:
			break;
		}
	}
	
}
