package com.app.nova.entity;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class Game {

	@Getter @Setter
	private List<Player> players;
	
	@Getter @Setter
	private boolean isDeuce;
	
	@Getter @Setter
	private boolean isFinish;
	
	public void iniciar(List<Player> players) {
		this.players=players;
		this.isDeuce = false;
		this.isFinish = false;
	}
}
