package com.app.nova.util;

public class Constants {

	public static enum STATUS {
		Love(0, "Love"), Fifteen(15, "Fifteen"), Thirty(30, "Thirty"), Forty(40, "Forty"), Advantage(1,"Advantage");

		private int codigo;
		private String message;

		private STATUS(int codigo, String message) {
			this.codigo = codigo;
			this.message = message;
		}

		public int getCodigo() {
			return codigo;
		}

		public String getMessage() {
			return message;
		}

	}
	
	
}
