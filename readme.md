# Project Code Challenge

_Proyecto Backend para la simulacion de Puntajes de los partidos de Tenis_


## Comenzando 🚀

_Para obtener una copia del repositorio solo se necesitaría clonar con el siguiente comando: ._

```
git clone https://azevallosyapias@bitbucket.org/azevallosyapias/tenis-challenge.git
```

### Pre-requisitos 📋

_Para poder desplegar el proyecto es necesario tener instalado:_

```
- Maven
- Java version 8 o superior

```


## Ejecutando las pruebas ⚙️

_Para ejecutar los UnitTest ejecutar el siguiente comando en la ubicacion del pom.xml_

```
mvn test
```


## Construido con 🛠️

_Herramientas utilizadas en el desarrollo del proyecto_

```
* Java 8
* Spring Framework
* Spring Boot
* Swagger: utilizado para la documentacion del contrato del API
* JUnit
* Mockito
* JavaDocs: utilizado para documentar el codigo
* Lombok
```


## Autor ✒️

* **Marino Alonso Z.Y. **

_Links:_
* [Linkedin](https://www.linkedin.com/in/marino-alonso-zevallos-yapias-82a200196/)
* Bitbucket : [azevallosyapias](https://bitbucket.org/azevallosyapias/)
* GitHub: [alonsozy](https://github.com/alonsozy)
* DockerHub: [alonsozy](https://hub.docker.com/u/alonsozy)



